CREATE TABLE [dbo].[Folder] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (MAX) NOT NULL,
    [ParentId] INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Child_ToParent] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Folder] ([Id])
);



SET IDENTITY_INSERT [dbo].[Folder] ON
INSERT INTO [dbo].[Folder] ([Id], [Name], [ParentId]) VALUES (1, N'Creating Digital Images', 1)
INSERT INTO [dbo].[Folder] ([Id], [Name], [ParentId]) VALUES (2, N'Resources', 1)
INSERT INTO [dbo].[Folder] ([Id], [Name], [ParentId]) VALUES (3, N'Evidence', 1)
INSERT INTO [dbo].[Folder] ([Id], [Name], [ParentId]) VALUES (4, N'Graphic Prosucts', 1)
INSERT INTO [dbo].[Folder] ([Id], [Name], [ParentId]) VALUES (5, N'Primary Sources', 2)
INSERT INTO [dbo].[Folder] ([Id], [Name], [ParentId]) VALUES (6, N'Secondary Sources', 2)
INSERT INTO [dbo].[Folder] ([Id], [Name], [ParentId]) VALUES (7, N'Process', 4)
INSERT INTO [dbo].[Folder] ([Id], [Name], [ParentId]) VALUES (8, N'Final Product', 4)
SET IDENTITY_INSERT [dbo].[Folder] OFF
