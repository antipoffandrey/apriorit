﻿using AprioritTestApp.Models;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace AprioritTestApp.Controllers
{
    public class HomeController : Controller
    {
        private EntityDataModel db = new EntityDataModel();      

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FindFolderByName(string id)
        {
            Folder folder;

            if (id == null)
            {
                /* выбираем root по признаку Id=ParentId, а затем прокидываем его в этот же метод, чтобы URL стал
                 типа  localhost/root. Это необходимо для корректной генерации ссылок для субдиректорий */

                folder = db.Folder.Where(i => i.Id == i.ParentId).Include(f => f.SubFolders).FirstOrDefault();
                return RedirectToAction("FindFolderByName", "Home", new { id = folder.Name });
            }
            else
            {
                var lastFolderName = id.Split('/').Last();
                folder = db.Folder.Where(c => c.Name == lastFolderName).FirstOrDefault();
            }

            if (folder != null)
                return View("Index", folder);
            else
                return HttpNotFound();
        }
    }
}