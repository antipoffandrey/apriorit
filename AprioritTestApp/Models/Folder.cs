namespace AprioritTestApp.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Folder")]
    public partial class Folder
    {
        public Folder()
        {
            SubFolders = new HashSet<Folder>();
        }

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int ParentId { get; set; }

        public virtual ICollection<Folder> SubFolders { get; set; }

        public virtual Folder ParentFolder { get; set; }

    }
}
