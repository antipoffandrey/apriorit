namespace AprioritTestApp.Models
{
    using System.Data.Entity;

    public partial class EntityDataModel : DbContext
    {
        public EntityDataModel()
            : base("name=EntityDataModel")
        {
        }

        public virtual DbSet<Folder> Folder { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Folder>()
                .HasMany(e => e.SubFolders)
                .WithRequired(e => e.ParentFolder)
                .HasForeignKey(e => e.ParentId);
        }
    }
}
